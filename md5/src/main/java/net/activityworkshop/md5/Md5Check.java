package net.activityworkshop.md5;

import java.io.File;
import java.io.FileReader;
import java.io.FileInputStream;
import java.io.BufferedReader;
import org.w3c.tools.crypt.Md5;

/**
 * Class to check an MD5 hash against an expected result
 * Uses the JigSaw implementation of the MD5 hash
 */
public class Md5Check
{
	/**
	 * Main method to parse the input parameters
	 * and pass to checking method
	 */
	public static void main(String[] args)
	{
		if (args.length == 0 || args.length > 2)
		{
			System.out.println("Usage: Md5Check <FileToHash> [CheckSum | CheckFile]");
		}
		else
		{
			String inputFilename = null;
			// Check whether file exists
			File inputFile = new File(args[0]);
			if (inputFile.exists() && inputFile.isFile() && inputFile.canRead())
			{
				inputFilename = args[0];
			}
			else
			{
				// file can't be read, exit
				System.out.println("Cannot read input file: " + args[0]);
				System.exit(0);
			}

			// Check the second input parameter for either a checksum
			// or a file containing the checksum
			String inChecksum = null;
			File inChecksumFile = null;
			if (args.length == 2)
			{
				// see if it's a file
				File checkFile = new File(args[1]);
				if (checkFile.exists() && checkFile.isFile() && checkFile.canRead())
				{
					inChecksumFile = checkFile;
				}
				else
				{
					inChecksum = args[1];
				}
			}
			checkFile(inputFilename, inChecksum, inChecksumFile);
		}
	}


	/**
	 * Check method, to generate the hash from the input file
	 * and compare with the expected hash
	 */
	private static void checkFile(String inInputFilename, String inChecksum, File inChecksumFile)
	{
		try
		{
			// Create the Md5 object and use it to generate the hash
			Md5 hasher = new Md5(new FileInputStream(new File(inInputFilename)));
			byte[] digest = hasher.getDigest();
			String hash = hasher.getStringDigest();
			System.out.println("Checksum of file '" + inInputFilename + "': " + hash);

			// check it against expected value (if given)
			if (inChecksum != null)
			{
				if (hash.equals(inChecksum))
					System.out.println("MATCH: hash matches parameter '" + inChecksum + "'");
				else
					System.out.println("FAILED: hash does not match parameter '" + inChecksum + "'");
			}

			// check it against value found in checksumfile (if given)
			if (inChecksumFile != null)
			{
				// Look inside file to see if key is present
				BufferedReader reader = new BufferedReader(new FileReader(inChecksumFile));
				// loop through each line of the file
				String checkLine = reader.readLine();
				boolean matchFound = false;
				while (checkLine != null)
				{
					if (checkLine.indexOf(hash) >= 0)
					{
						matchFound = true;
						// hash found in checkfile - check filename too
						if (checkLine.indexOf(new File(inInputFilename).getName()) >= 0)
						{
							// Found filename as well
							System.out.println("MATCH: Hash and filename: " + checkLine);
						}
						else
						{
							if (checkLine.equals(hash))
								System.out.println("MATCH: hash found: " + checkLine);
							else
								System.out.println("PARTIAL MATCH: " + checkLine);
						}
					}
					checkLine = reader.readLine();
				}
				if (!matchFound)
					System.out.println("FAILED: computed hash not found in file: " + inChecksumFile.getName());
			}
		}
		catch (java.io.IOException e)
		{
			System.out.println("Error creating hash");
		}
	}
}