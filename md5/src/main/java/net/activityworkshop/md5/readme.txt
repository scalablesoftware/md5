MD5CHECK README
===============

Recommended usage:   java -jar md5.jar <filename>
                     java -jar md5.jar <filename> [expected-checksum]
                     java -jar md5.jar <filename> [checksum-filename]

This jar file contains two java classes for the generation and verification of
MD5 checksums, also known as hashes.  These are alphanumeric strings calculated
from the contents of a file.

One primary application is to verify that a given file was successfully
downloaded without being corrupted or truncated along the way.  In this case
the provider of the file would also provide the calculated checksum so that
each recipient may compare the checksum generated from his/her file with the
expected value.  If the hashes match, it's very unlikely that an accidental
error occurred during transmission.

This jar provides a similar functionality to the unix utility md5sum, but in a
cross-platform way.


The Jigsaw Implementation
-------------------------
The class which does all the gruntwork is Md5, taken originally from w3.org's
Jigsaw project (http://w3.org/jigsaw).  This does the actual
checksum calculations, and if desired can be called directly as follows:
    java org.w3c.tools.crypt.Md5 <filename>
where <filename> determines the file from which to generate the checksum.  In
this case the generated checksum is simply output to the console.


The Verification Wrapper
------------------------
The second class, Md5Check, provides a wrapper around the Md5 class and
provides the option to check against an expected checksum.  This is easier and
less error prone than outputting both to the screen and checking by eye.  The
expected checksum can be given as an additional command line parameter like so:
    java md5.Md5Check <filename> [expected-checksum]
where the [expected-checksum] can be copied and pasted from the provided value.

Alternatively a second filename can be given, like so;
    java md5.Md5Check <filename> [checksum-filename]
where [checksum-filename] determines a file containing the expected checksums.
In this case the checksum file may contain checksums for more than one file, in
which case they are expected to be written one per line with the checksum and
filename given on each line.


Output of Md5Check
------------------
Md5Check also sends its output to System.out, which is normally the console.
Firstly the checksum is displayed, and if a subsequent parameter was given then
the checksums are compared.
If the second command line parameter corresponds to a readable file, this is
opened and its contents are searched for the calculated checksum.  If the
checksum is found, the full line from the checksum file is shown for
verification, and a message indicates whether a 'MATCH' was found, complete
with correct filename, or merely a 'PARTIAL MATCH', where the filename was
not found (either no filename, or a different filename was given).  Absence of
the checksum from the file gives a 'FAILED' message.
If the second command line parameter does not correspond to a file, it is
treated as an expected checksum value, and is compared with the calculated
value.  This results in either a 'MATCH' message or a 'FAILED' message.


Links & Credits
---------------
The Md5 class was written by w3.org's Jigsaw project: http://w3.org/jigsaw/
and taken from the 4.3MB download of v2.2.4.
The Md5Check wrapper and this README were written by activityworkshop:
   http://activityworkshop.net

Distribution of this work in whole or in part is encouraged in any form for
any purpose.  The Md5 class is the property of w3c, and copyrighted thusly:
   Copyright � World Wide Web Consortium, (Massachusetts Institute of
   Technology, Institut National de Recherche en Informatique et en
   Automatique, Keio University). All Rights Reserved.
   http://www.w3.org/Consortium/Legal/